import { postAddProduct, postPromo, putFeatureItem, deleteFeatureItem } from "../services/admin.services";
import { sendCallback, checkError } from "../utils/constants";
import { changeProductByType } from "./user.actions";

export const addProductAction = (info, callback) => async dispatch => {
    try {
        const { title, description, price, artist, images, discount } = info;
        const formData = new FormData();
        if (Array.isArray(images)) {
            images.forEach(image => {
                formData.append('images', image);
            });
        }
        else formData.append('images', images);
        formData.append('title', title);
        formData.append('description', description)
        formData.append('price', price)
        formData.append('artist', artist);
        formData.append('discount', discount);
        await postAddProduct(formData);
        sendCallback(callback);

    } catch (error) {
        checkError(error, callback);
    }
}

export const addPromoAction = (productInfo, callback) => async (dispatch, getState) => {
    try {
        await postPromo(productInfo);
        sendCallback(callback);
    } catch (error) {
        checkError(error, callback);
    }
}

export const featureItemAction = (productInfo, callback) => async (dispatch, getState) => {
    try {
        const { productId, type } = productInfo;
        await putFeatureItem(productId);
        dispatch(changeProductByType(type, productId, 'featured', true));
        sendCallback(callback);
    } catch (error) {
        checkError(error, callback)
    }
}

export const unFeatureItemAction = (productInfo, callback) => async (dispatch, getState) => {
    try {
        const { productId, type } = productInfo;
        await deleteFeatureItem(productId);
        dispatch(changeProductByType(type, productId, 'featured', false));
        sendCallback(callback);
    } catch (error) {
        checkError(error, callback)
    }
}