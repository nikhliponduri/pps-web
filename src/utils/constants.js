export const SUCCESS = 'SUCCESS';
export const FAILURE = 'FAILURE';
export const NETWORK_ERROR = 'Please check your network conection';
export const S3_URL = 'https://projectpps.s3.ap-south-1.amazonaws.com';
export const LATEST_PRODUCTS = 'LATEST_PRODUCTS';
export const PRODUCT_DETAIL = 'PRODUCT_DETAIL';
export const ALL_PRODUCTS = 'ALL_PRODUCTS';
export const HOME_PRODUCTS = 'HOME_PRODUCTS';
export const LIKED_PRODUCTS = 'LIKED_PRODUCTS';

export const checkError = (error, callback) => {
    console.log(error);
    if (!error.response || !error.response.status) return callback && callback({
        type: FAILURE,
        message: NETWORK_ERROR
    });
    return callback && callback({
        type: FAILURE,
        message: error.response.data.message
    })
}

export const sendCallback = (callback) => {
    callback && callback({
        type: SUCCESS
    })
}