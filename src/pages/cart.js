import React, { Fragment } from 'react';
import { Link } from 'react-router-dom';
import AppLoader from '../components/shared/app-loader';
import { checkLoading, getImageUrl } from '../utils/util';

const Cart = (props) => {
    const { cart, removeFromCartAction } = props;
    let grandTotal;
    if (Array.isArray(cart)) {
        grandTotal = cart.reduce((total, current) => current.data.price - current.data.discount, 0);
    }
    return (
        <div>
            <section id="aa-catg-head-banner">
                <img src="img/fashion/fashion-header-bg-8.jpg" alt="fashion img" />
                <div className="aa-catg-head-banner-area">
                    <div className="container">
                        <div className="aa-catg-head-banner-content">
                            <h2>Cart Page</h2>
                            <ol className="breadcrumb">
                                <li><Link to='/'>Home</Link></li>
                                <li className="active">Cart</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </section>
            <section id="cart-view">
                <div className="container">
                    <div className="row">
                        <div className="col-md-12">
                            <div className="cart-view-area">
                                <AppLoader loading={checkLoading(cart)} failType='Error fetchng details' parent>
                                    {cart && cart.length > 0 ?
                                        <div className="cart-view-table">
                                            <form action="">
                                                <div className="table-responsive">
                                                    <table className="table">
                                                        <thead>
                                                            <tr>
                                                                <th></th>
                                                                <th></th>
                                                                <th>Product</th>
                                                                <th>Price</th>
                                                                <th>Quantity</th>
                                                                <th>Total</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            {cart && cart.map(item => {
                                                                const { data: { _id, price, discount, title, images } } = item;
                                                                return (
                                                                    <tr>
                                                                        <td><Link className="remove" onClick={() => removeFromCartAction({ productId: _id })}><fa className="fa fa-close"></fa></Link></td>
                                                                        <td><Link to={`/product-detail/${_id}`}><img src={getImageUrl(images[0])} alt={title} /></Link></td>
                                                                        <td><Link className="aa-cart-title" href="#">{title}</Link></td>
                                                                        <td>{price - discount}</td>
                                                                        <td><input className="aa-cart-quantity" type="number" value="1" disabled /></td>
                                                                        <td>{price - discount}</td>
                                                                    </tr>
                                                                )
                                                            })}
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </form>
                                            {/* <!-- Cart Total view --> */}
                                            < div className="cart-view-total">
                                                <h4>Cart Totals</h4>
                                                <table className="aa-totals-table">
                                                    <tbody>
                                                        <tr>
                                                            <th>Subtotal</th>
                                                            <td>{grandTotal}</td>
                                                        </tr>
                                                        <tr>
                                                            <th>Total</th>
                                                            <td>{grandTotal}</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <Link to='/checkout' className="aa-cart-view-btn">Proced to Checkout</Link>
                                            </div>
                                        </div> : <div className='text-center'>Your shopping cart is empty</div>
                                    }
                                </AppLoader>
                            </div>
                        </div>

                    </div>
                </div>
            </section>
        </div >
    )
}

export default Cart
