import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import FormInput from '../components/shared/form-input';
import { isValidLoginForm } from '../validation/validations';
import { resolveByType } from '../utils/util';
import { toast } from 'react-toastify';
import Button from '../components/shared/button';

const Login = ({ userLoginAction }) => {
    const [loginInfo, setLoginInfo] = useState({
        email: '',
        password: ''
    });
    const [formErrors, setFormErrors] = useState({});
    const [loading, setLoading] = useState(false);
    const handleChange = (e) => {
        setLoginInfo({
            ...loginInfo,
            [e.target.name]: e.target.value
        });
    }
    const loginClose = () => {
        document.getElementById('login-close').click();
    }
    const serverCallback = ({ type, message }) => {
        setLoading(false);
        resolveByType({
            type,
            success: () => loginClose(),
            failure: () => {
                toast.error(message);
            }
        })
    }
    const submit = (e) => {
        e.preventDefault();
        const validation = isValidLoginForm(loginInfo);
        if (!validation.isValidForm) return setFormErrors(validation.errors);
        setLoading(true);
        setFormErrors({});
        userLoginAction(loginInfo, serverCallback);
    }
    return (
        <div className="modal fade" id="login-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div className="modal-dialog">
                <div className="modal-content">
                    <div className="modal-body">
                        <button id='login-close' type="button" className="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 classNameName='text-center'>Login</h4>
                        <form className="aa-login-form">
                            <div>
                                <label for="">Username or Email address<span>*</span></label>
                                <FormInput name='email' onChange={handleChange} value={loginInfo.email} error={formErrors.email} style={{ marginBottom: 0 }} type="text" placeholder="Username or email" />
                            </div>
                            <div>
                                <label for="">Password<span>*</span></label>
                                <FormInput name='password' value={loginInfo.password} error={formErrors.password} onChange={handleChange} style={{ marginBottom: 0 }} type="password" placeholder="Password" />
                            </div>
                            <Button loading={loading} onClick={submit} className="aa-browse-btn" title='Login' />
                            <label for="rememberme" className="rememberme"><input type="checkbox" id="rememberme" /> Remember me </label>
                            <p className="aa-lost-password"><Link onClick={loginClose} to='/forgot-password' >Lost your password?</Link></p>
                            <div className="aa-register-now">
                                Don't have an account?<Link onClick={loginClose} to='/account'>Register now!</Link>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Login
