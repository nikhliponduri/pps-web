import React, { useState } from 'react';
import { Link, withRouter } from 'react-router-dom';
import FormInput from '../components/shared/form-input';
import Button from '../components/shared/button';
import { isValidLoginForm, isValidRegisterForm } from '../validation/validations';
import { resolveByType, windowLocation } from '../utils/util';
import { toast } from 'react-toastify';

const Account = ({ userLoginAction, userRegisterAction, history: { push } }) => {
    const [loading, setLoading] = useState(false);
    const [formErrors, setFormErrors] = useState({});
    const [registerFormErrors, setRegisterFormErrors] = useState({});
    const [registerInfo, setRegisterinfo] = useState({
        email: '',
        password: ''
    })
    const [loginInfo, setLoginInfo] = useState({
        email: '',
        password: ''
    })
    const handleChange = (e) => {
        setLoginInfo({
            ...loginInfo,
            [e.target.name]: e.target.value
        });
    }
    const serverCallback = ({ type, message }) => {
        setLoading(false);
        resolveByType({
            type,
            success: () => {
                if (windowLocation().split('/').pop() === 'account') push('/');
            },
            failure: () => {
                toast.error(message);
            }
        })
    }
    const submit = (e) => {
        e.preventDefault();
        const validation = isValidLoginForm(loginInfo);
        if (!validation.isValidForm) return setFormErrors(validation.errors);
        setLoading(true);
        setFormErrors({});
        userLoginAction(loginInfo, serverCallback);
    }
    const registerCallback = ({ type, message }) => {
        setLoading(false);
        resolveByType({
            type,
            success: () => {
                toast.success('Please login with your credentials');
                setRegisterinfo({
                    email: '',
                    password: ''
                })
            },
            failure: () => toast.error(message)
        })
    }
    const registerSubmit = (e) => {
        e.preventDefault();
        const validation = isValidRegisterForm(registerInfo);
        if (!validation.isValidForm) return setRegisterFormErrors(validation.errors);
        setLoading(true);
        setRegisterFormErrors({});
        userRegisterAction(registerInfo, registerCallback);
    }
    const handleRegisterChange = (e) => {
        setRegisterinfo({
            ...registerInfo,
            [e.target.name]: e.target.value
        })
    }
    return (
        <section id="aa-myaccount">
            <div className="container">
                <div className="row">
                    <div className="col-md-12">
                        <div className="aa-myaccount-area">
                            <div className="row">
                                <div className="col-md-6">
                                    <div className="aa-myaccount-login">
                                        <h4>Login</h4>
                                        <form action="" className="aa-login-form">
                                            <div>
                                                <label for="">Username or Email address<span>*</span></label>
                                                <FormInput name='email' onChange={handleChange} value={loginInfo.email} error={formErrors.email} style={{ marginBottom: 0 }} type="text" placeholder="Username or email" />
                                            </div>
                                            <div>
                                                <label for="">Password<span>*</span></label>
                                                <FormInput name='password' value={loginInfo.password} error={formErrors.password} onChange={handleChange} style={{ marginBottom: 0 }} type="password" placeholder="Password" />
                                            </div>
                                            <Button disabled={loading} loading={loading} onClick={submit} className="aa-browse-btn" title='Login' />
                                            <label className="rememberme" for="rememberme"><input type="checkbox" id="rememberme" /> Remember me </label>
                                            <p className="aa-lost-password"><Link to='/forgot-password'>Lost your password?</Link></p>
                                        </form>
                                    </div>
                                </div>
                                <div className="col-md-6">
                                    <div className="aa-myaccount-register">
                                        <h4>Register</h4>
                                        <form action="" className="aa-login-form">
                                            <div>
                                                <label for="">Username or Email address<span>*</span></label>
                                                <FormInput onChange={handleRegisterChange} style={{ marginBottom: 0 }} name='email' error={registerFormErrors.email} value={registerInfo.email} type="text" placeholder="Username or email" />
                                            </div>
                                            <div>
                                                <label for="">Password<span>*</span></label>
                                                <FormInput onChange={handleRegisterChange} style={{ marginBottom: 0 }} name='password' error={registerFormErrors.password} value={registerInfo.password} type="password" placeholder="Password" />
                                            </div>
                                            <Button onClick={registerSubmit} disabled={loading} loading={loading} className="aa-browse-btn" title='Register' />
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default withRouter(Account);
