import React, { Fragment } from 'react'
import ProductLens from '../containers/common/product-lens';
import Login from '../containers/auth/login';

const DefaultHTMLS = () => {
    return (
        <Fragment>
            <Login />
            <ProductLens />
        </Fragment>

    )
}

export default DefaultHTMLS
