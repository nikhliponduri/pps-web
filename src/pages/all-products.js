import React, { useEffect } from 'react'
import AppLoader from '../components/shared/app-loader';
import { checkLoading } from '../utils/util';
import ProductList from './product-list';
import Pagination from './pagination';

const AllProducts = (props) => {
    const { allProductsAction, allProducts, type,loggedIn } = props;
    useEffect(() => {
        allProductsAction();
    }, [allProductsAction,loggedIn]);
    const onPageClick = (page) => {
        allProductsAction({ query:`page=${page}&offset=12` });
    }
    return (
        <section id="aa-product">
            <div className="container">
                <div className="row">
                    <div className="col-md-12">
                        <div className="row">
                            <div className="aa-product-area">
                                <div className="aa-product-inner all-products">
                                    <ul className="nav nav-tabs aa-products-tab">
                                        <li className="active"><a data-toggle="tab">All Products</a></li>
                                    </ul>
                                    <AppLoader loading={checkLoading(allProducts.data)} failType='Error loading' parent>
                                        <ProductList products={allProducts.data} type={type} />
                                    </AppLoader>
                                </div>
                            </div>
                        </div>
                    </div>
                    {allProducts.data &&
                        <Pagination {...allProducts.metadata[0]} onPageClick={onPageClick} />
                    }
                </div>
            </div>
        </section>
    )
}

export default AllProducts
