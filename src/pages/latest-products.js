import React, { useEffect } from 'react';
import { Link } from 'react-router-dom';
import ProductList from './product-list';
import AppLoader from '../components/shared/app-loader';
import { checkLoading } from '../utils/util';

const LatestProducts = (props) => {
    const { latestProducts, latestProductsAction, type,loggedIn } = props;
    useEffect(() => {
        latestProductsAction()
    }, [latestProductsAction,loggedIn]);
    return (
        <section id="aa-popular-category">
            <div className="container">
                <div className="row">
                    <div className="col-md-12">
                        <div className="row">
                            <div className="aa-popular-category-area">
                                {/* <!-- start prduct navigation --> */}
                                <ul className="nav nav-tabs aa-products-tab">
                                    <li className='active'><Link to='/'>Latest</Link></li>
                                </ul>
                                {/* <!-- Tab panes --> */}
                                <AppLoader loading={checkLoading(latestProducts)} failType='Coudnt load now' parent>
                                    <ProductList products={latestProducts} type={type} />
                                </AppLoader>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default LatestProducts
