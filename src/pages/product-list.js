import React, { Component, Fragment } from 'react';
import { Link } from 'react-router-dom'
import ProductCard from '../containers/common/product-card';
import { LATEST_PRODUCTS } from '../utils/constants';

class ProductList extends Component {
    componentDidMount() {
        const { type } = this.props;
        type === LATEST_PRODUCTS && window.popularSlider && window.popularSlider();
    }

    render() {
        const { products, type } = this.props;
        return (
            <Fragment>
                <div className="tab-content">
                    <div className="tab-pane fade in active" id="popular">
                        <ul className={`aa-product-catg ${type === LATEST_PRODUCTS && 'aa-popular-slider'}`}>
                            {
                                products.map(product => <ProductCard product={{ ...product }} type={type} />)
                            }
                        </ul>
                        {/* <Link className="aa-browse-btn" to='/all-products'>Browse all Product <span className="fa fa-long-arrow-right"></span></Link> */}
                    </div>
                </div>
            </Fragment>
        )
    }
}

export default ProductList
