import React, { useEffect } from 'react'
import AppLoader from '../components/shared/app-loader';
import { checkLoading, windowLocation } from '../utils/util';
import ProductDetail from './product-detail';

const ProductDetailLoader = (props) => {
    const { productDetailAction,loggedIn } = props;
    useEffect(() => {
        productDetailAction({ productId: windowLocation().split('/').pop() });
    }, [productDetailAction,loggedIn]);
    return (
        <section id="aa-product-details">
            <div className="container">
                <div className="row">
                    <AppLoader loading={checkLoading(props.product)} failType='Error loading' parent>
                        <ProductDetail {...props} />
                    </AppLoader>
                </div>
            </div>
        </section>
    )
}

export default ProductDetailLoader
