import React, { useState } from 'react'
import FormInput from '../../components/shared/form-input';
import SelectInput from '../../components/shared/select-input';
import Button from '../../components/shared/button';
import { resolveByType } from '../../utils/util';
import { toast } from 'react-toastify';

const AddPromo = ({ addPromoAction }) => {
    const [productInfo, setProductInfo] = useState({
        productId: '',
        promo: 1
    });
    const [loading, setLoading] = useState(false);
    const [formErrors, setFormErrors] = useState({});
    const handleChange = (e) => {
        setProductInfo({
            ...productInfo,
            [e.target.name]: e.target.value
        });
    }
    const serverCallback = ({ type, message }) => {
        setLoading(false);
        resolveByType({
            type,
            success: () => {
                setProductInfo({
                    productId: ''
                });
            },
            failure: () => {
                toast.error(message);
            }
        })
    }
    const submit = (e) => {
        e.preventDefault();
        if (productInfo.productId.match(/^[0-9a-fA-F]{24}$/)) {
            setLoading(true);
            setFormErrors({});
            addPromoAction(productInfo, serverCallback);
            return;
        }
        setFormErrors({
            productId: 'Please select a valid productId'
        })
    }
    return (
        <section id="aa-myaccount" >
            <div className="container">
                <div className="row">
                    <div className="col-md-12">
                        <div className="aa-myaccount-area">
                            <h4 className='text-center'>Add Promo</h4>
                            <form action="" className="aa-login-form">
                                <div className='row'>
                                    <div className="col-md-4">
                                        <div className="aa-myaccount-login">
                                            <div>
                                                <label for="">Product Id<span>*</span></label>
                                                <FormInput value={productInfo.productId} error={formErrors.productId} onChange={handleChange} name='productId' style={{ marginBottom: 0 }} type="text" placeholder="title" />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-4">
                                        <div className="aa-myaccount-login">
                                            <div>
                                                <div className='row'>
                                                    <label for="">Promo<span>*</span></label>
                                                </div>
                                                <div className='row'>
                                                    <SelectInput value={productInfo.promo} options={[{ value: 1, label: 1 }, { value: 2, label: 2 }, { value: 3, label: 3 }, { value: 4, label: 4 }, { value: 5, label: 5 }]} error={formErrors.title} onChange={(e) => {
                                                        setProductInfo({
                                                            ...productInfo,
                                                            promo: e.target.value
                                                        })
                                                    }} name='promo' style={{ marginBottom: 0 }} type="text" placeholder="title" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className='row'>
                                    <div className="col-md-7">
                                        <Button loading={loading} disabled={loading} onClick={submit} className='aa-browse-btn' title='Submit' />
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default AddPromo
