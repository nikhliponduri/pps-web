import React, { useState } from 'react';
import FormInput from '../../components/shared/form-input';
import Button from '../../components/shared/button';
import TextArea from '../../components/shared/text-area';
import { isValidAddProductform } from '../../validation/validations';
import { resolveByType } from '../../utils/util';
import { toast } from 'react-toastify';

const AddProduct = ({ addProductAction }) => {
    const [productInfo, setProductInfo] = useState({
        title: '',
        description: '',
        artist: '',
        discount: '',
        price: '',
        images: []
    });
    const [preview, setPreview] = useState([]);
    const [formErrors, setFormErrors] = useState({});
    const [loading, setLoading] = useState(false);
    const handleChange = (e) => {
        setProductInfo({
            ...productInfo,
            [e.target.name]: e.target.value
        })
    }
    const serverCallback = ({ type, message }) => {
        setLoading(false);
        resolveByType({
            type,
            success: () => {
                toast.success('Product added sucessfully');
                setPreview([]);
                setProductInfo({
                    title: '',
                    images: '',
                    description: '',
                    price: '',
                    discount: ''
                });
            },
            failure: () => {
                toast.error(message);
            }
        })
    }
    const submit = (e) => {
        e.preventDefault();
        const validation = isValidAddProductform(productInfo);
        if (!validation.isValidForm) return setFormErrors(validation.errors);
        setFormErrors({});
        setLoading(true);
        addProductAction(productInfo, serverCallback)
    }
    const handleImage = (e) => {
        const { files } = e.target
        if (files && files[0] && files[0].type.split('/')[0] === 'image') {
            setProductInfo({
                ...productInfo,
                images: [...productInfo.images, files[0]]
            });
            const reader = new FileReader();
            reader.onloadend = (e) => {
                setPreview([...preview, e.target.result]);
            }
            reader.readAsDataURL(files[0]);
        }
    }
    const deleteImage = (i) => {
        preview.splice(i, 1);
        productInfo.images.splice(i, 1);
        setPreview([...preview]);
        setProductInfo({
            ...productInfo,
            images: [...productInfo.images]
        });
    }
    return (
        <section id="aa-myaccount" >
            <div className="container">
                <div className="row">
                    <div className="col-md-12">
                        <div className="aa-myaccount-area">
                            <h4 className='text-center'>Add Product</h4>
                            <form action="" className="aa-login-form">
                                <div className='row'>
                                    <div className="col-md-4">
                                        <div className="aa-myaccount-login">
                                            <div>
                                                <label for="">Title<span>*</span></label>
                                                <FormInput value={productInfo.title} error={formErrors.title} onChange={handleChange} name='title' style={{ marginBottom: 0 }} type="text" placeholder="title" />
                                            </div>
                                            <div>
                                                <label for="">Artist<span>*</span></label>
                                                <FormInput value={productInfo.artist} error={formErrors.artist} onChange={handleChange} name='artist' style={{ marginBottom: 0 }} type="text" placeholder="artist" />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-4">
                                        <div className="aa-myaccount-register">
                                            <div>
                                                <label for="">Price</label>
                                                <FormInput value={productInfo.price} error={formErrors.price} onChange={handleChange} style={{ marginBottom: 0 }} name='price' type="text" placeholder="price" />
                                            </div>
                                            <div>
                                                <label for="">Discount</label>
                                                <FormInput value={productInfo.discount} error={formErrors.discount} onChange={handleChange} style={{ marginBottom: 0 }} name='discount' type="text" placeholder="discount" />
                                            </div>
                                        </div>
                                    </div>
                                    <div className='col-md-12'>
                                        <div className="col-md-7" style={{ marginTop: '20px' }}>
                                            <div className="aa-myaccount-register">
                                                <div>
                                                    <label for="">Description<span>*</span></label>
                                                    <TextArea value={productInfo.description} error={formErrors.description} onChange={handleChange} rows={6} style={{ width: '100%' }} name='description' type="text" placeholder="Description" />
                                                </div>
                                            </div>
                                            <div className="aa-myaccount-register">
                                                <div>
                                                    <label for="">Images<span>*</span></label>
                                                    <input disabled={preview.length > 3} error={formErrors.images} onChange={handleImage} style={{ marginBottom: 0 }} name='images' type="file" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-7">
                                        <Button loading={loading} disabled={loading || !productInfo.images.length} onClick={submit} className='aa-browse-btn' title='Submit' />
                                    </div>
                                </div>
                            </form>
                            {preview.length > 0 &&
                                <div className='col-md-12' style={{ marginTop: '10px' }}>
                                    <div class="row image-preview">
                                        <div id="small-img" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 center">
                                            <ul>
                                                {preview.map((image, i) =>
                                                    <li>
                                                        <img src={image} className="img-responsive inline-block image-preview" alt="" />
                                                        <i className='fas fa-times pointer' style={{ position: 'absolute', top: '0', right: '0', color: 'red' }} onClick={(e) => deleteImage(i)}></i>
                                                    </li>
                                                )}
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            }
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default AddProduct
