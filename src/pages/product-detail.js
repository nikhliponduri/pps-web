import React, { useState } from 'react';
import ImageMagnify from 'react-image-magnify';
import { Link } from 'react-router-dom';
import ProductLens from '../components/shared/product-lens';
import { getImageUrl, clickLogin, resolveByType } from '../utils/util';
import { toast } from 'react-toastify';

const ProductDetail = (props) => {
    const { product: { title, images, artist, description, price, discount, _id, liked, inCart }, addToCartAction, removeFromCartAction, likeProductAction, removeLikeAction, productDetailAction, loggedIn, type } = props;
    const [imageUrl, setImageUrl] = useState(images[0]);
    const [zIndex, setZindex] = useState(1);
    const cartCallback = ({ type, message }) => {
        resolveByType({
            type,
            success: () => {
                toast.success('Added to cart');
            },
            failure: () => {
                toast.error(message);
            }
        })
    }
    const removeCartCallback = ({ type, message }) => {
        resolveByType({
            type,
            success: () => {
                toast.success('Removed from cart');
            },
            failure: () => {
                toast.error(message);
            }
        })
    }
    const likeCallback = ({ type, message }) => {
        resolveByType({
            type,
            sucess: () => toast.success('Added to whishlist'),
            failure: () => toast.error(message)
        })
    }
    const handleCart = (e) => {
        e.stopPropagation();
        if (!loggedIn) return clickLogin();
        if (!inCart) return addToCartAction({ productId: _id, type }, cartCallback);
        removeFromCartAction({ productId: _id, type }, removeCartCallback);
    }
    const handleLike = (e) => {
        e.stopPropagation();
        if (!loggedIn) return clickLogin();
        if (!liked) return likeProductAction({ productId: _id, type }, likeCallback);
        removeLikeAction({ productId: _id, type }, likeCallback);
    }
    return (
        <div className="col-md-12">
            <div className="aa-product-details-area">
                <div className="aa-product-details-content">
                    <div className="row">
                        {/* <!-- Modal view slider --> */}
                        <div className="col-md-5 col-sm-5 col-xs-12">
                            <div className="aa-product-view-slider">
                                <div id="demo-1" className="simpleLens-gallery-container">
                                    <div className="simpleLens-container">
                                        <div className="simpleLens-big-image-container"><Link className="simpleLens-lens-image" onMouseEnter={() => setZindex(-1)} onMouseLeave={() => setZindex(1)}>
                                            <ImageMagnify {...{
                                                smallImage: {
                                                    alt: 'Wristwatch by Ted Baker London',
                                                    isFluidWidth: true,
                                                    src: getImageUrl(imageUrl),
                                                },
                                                largeImage: {
                                                    src: getImageUrl(imageUrl),
                                                    width: 2000,
                                                    height: 2000
                                                }
                                            }} /></Link></div>
                                    </div>
                                    <div className="simpleLens-thumbnails-container">
                                        {images.map(image =>
                                            <Link onMouseEnter={() => setImageUrl(image)} onClick={() => setImageUrl(image)} className="simpleLens-thumbnail-wrapper" href="#">
                                                <img src={getImageUrl(image)} alt={title} />
                                            </Link>
                                        )}
                                    </div>
                                </div>
                            </div>
                        </div>
                        {/* <!-- Modal view content --> */}
                        <div className="col-md-7 col-sm-7 col-xs-12" style={{ zIndex }}>
                            <div className="aa-product-view-content">
                                <h3>{title}</h3>
                                <div className="aa-price-block">
                                    <span className="aa-product-view-price">{price - discount}</span>
                                    {/* <p className="aa-product-avilability">Avilability: <span>In stock</span></p> */}
                                </div>
                                <p>{description}</p>
                                {/* <h4>Size</h4>
                                <div className="aa-prod-view-size">
                                    <Link href="#">S</Link>
                                    <Link href="#">M</Link>
                                    <Link href="#">L</Link>
                                    <Link href="#">XL</Link>
                                </div>
                                <h4>Color</h4>
                                <div className="aa-color-tag">
                                    <Link href="#" className="aa-color-green"></Link>
                                    <Link href="#" className="aa-color-yellow"></Link>
                                    <Link href="#" className="aa-color-pink"></Link>
                                    <Link href="#" className="aa-color-black"></Link>
                                    <Link href="#" className="aa-color-white"></Link>
                                </div> */}
                                <div className="aa-prod-quantity">
                                    {/* <form action="">
                                        <select id="" name="">
                                            <option selected="1" value="0">1</option>
                                            <option value="1">2</option>
                                            <option value="2">3</option>
                                            <option value="3">4</option>
                                            <option value="4">5</option>
                                            <option value="5">6</option>
                                        </select>
                                    </form> */}
                                    <p className="aa-prod-category">
                                        Art By: <Link href="#">{artist}</Link>
                                    </p>
                                </div>
                                <div className="aa-prod-view-bottom">
                                    <Link className="aa-add-to-cart-btn" onClick={handleCart}>{inCart ? 'Remove From Cart' : 'Add To Cart'}</Link>
                                    <Link className="aa-add-to-cart-btn" onClick={handleLike} ><span className={`fas fa-heart ${liked && 'red'}`}></span></Link>
                                    {/* <Link className="aa-add-to-cart-btn" href="#">Compare</Link> */}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="aa-product-details-bottom">
                    <ul className="nav nav-tabs" id="myTab2">
                        <li><Link href="#description" >Description</Link></li>
                    </ul>

                    {/* <!-- Tab panes --> */}
                    <div className="tab-content">
                        <div className="tab-pane fade in active" id="description">
                            {description}
                        </div>
                    </div>
                </div>
                {/* <!-- Related product --> */}
            </div>
        </div>

    )
}

export default ProductDetail
