import React from 'react'

const RecentProducts = () => {
    return (
        <div className="col-md-3">
            <aside className="aa-blog-sidebar">
                {/* <div className="aa-sidebar-widget">
                    <h3>Category</h3>
                    <ul className="aa-catg-nav">
                        <li><a href="#">Men</a></li>
                        <li><a href="">Women</a></li>
                        <li><a href="">Kids</a></li>
                        <li><a href="">Electornics</a></li>
                        <li><a href="">Sports</a></li>
                    </ul>
                </div>
                <div className="aa-sidebar-widget">
                    <h3>Tags</h3>
                    <div className="tag-cloud">
                        <a href="#">Fashion</a>
                        <a href="#">Ecommerce</a>
                        <a href="#">Shop</a>
                        <a href="#">Hand Bag</a>
                        <a href="#">Laptop</a>
                        <a href="#">Head Phone</a>
                        <a href="#">Pen Drive</a>
                    </div>
                </div> */}
                <div className="aa-sidebar-widget">
                    <h3>Recent Post</h3>
                    <div className="aa-recently-views">
                        <ul>
                            <li>
                                <a className="aa-cartbox-img" href="#"><img src="img/woman-small-2.jpg" alt="img" /></a>
                                <div className="aa-cartbox-info">
                                    <h4><a href="#">Lorem ipsum dolor sit amet.</a></h4>
                                    <p>March 26th 2016</p>
                                </div>
                            </li>
                            <li>
                                <a className="aa-cartbox-img" href="#"><img src="img/woman-small-1.jpg" alt="img" /></a>
                                <div className="aa-cartbox-info">
                                    <h4><a href="#">Lorem ipsum dolor.</a></h4>
                                    <p>March 26th 2016</p>
                                </div>
                            </li>
                            <li>
                                <a className="aa-cartbox-img" href="#"><img src="img/woman-small-2.jpg" alt="img" /></a>
                                <div className="aa-cartbox-info">
                                    <h4><a href="#">Lorem ipsum dolor.</a></h4>
                                    <p>March 26th 2016</p>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </aside>
        </div>
    )
}

export default RecentProducts
