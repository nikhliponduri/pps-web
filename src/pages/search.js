import React, { Component } from 'react';
import qs from 'query-string';
import { withRouter } from 'react-router-dom';
import AppLoader from '../components/shared/app-loader';
import { checkLoading } from '../utils/util';
import ProductList from './product-list';
import Pagination from './pagination';

class Search extends Component {
    constructor(props) {
        super(props);
        this.state = {
            search: ''
        }
    }
    componentWillMount() {
        const { location: { search }, allProductsAction } = this.props;
        const query = qs.parse(search);
        allProductsAction({ query: `name=${query.name}` });
        this.setState({
            search: query.name
        })
    }
    componentWillReceiveProps(nextProps) {
        const { search } = this.state;
        const { location, allProductsAction } = nextProps;
        const query = qs.parse(location.search);
        if (query.name !== search || nextProps.loggedIn !== this.props.loggedIn) {
            allProductsAction({ query: `name=${query.name}` });
            this.setState({
                search: query.name
            })
        }
    }
    onPageClick(page) {
        const { allProductsAction, location: { search } } = this.props;
        allProductsAction({ query: `${search}&page=${page}` });
    }
    render() {
        const { allProducts: { data, metadata }, type } = this.props;
        if (Array.isArray(data) && data.length === 0) {
            return <div className='text-center'>
                <b> No products matched with your search results</b>
            </div>
        }
        return (
            <section id="aa-product">
                <div className="container" style={{ marginTop: '30px' }}>
                    <div className="row">
                        <div className="col-md-12">
                            <div className="row">
                                <div className="aa-product-area">
                                    <div className="aa-product-inner all-products">
                                        <AppLoader loading={checkLoading(data)} failType='Coudnt fetch now' parent>
                                            <ProductList type={type} products={data} />
                                        </AppLoader>
                                        {data && <Pagination {...metadata[0]} onPageClick={this.onPageClick} />}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        )
    }
}

export default withRouter(Search)
