import React from 'react';
import { withRouter } from 'react-router-dom';

const Pagination = (props) => {
    const { page, offset = 12, total, onPageClick } = props;
    const pages = [];
    for (let i = 0; (i < (Math.ceil(total / offset) - (page + 1))) && (i < 3); i++) {
        pages.push(<li key={i} className='pagination__item pointer' onClick={(e) => {
            onPageClick(page + 1 + i)
        }}><a>{page + 2 + i}</a></li>)
    }
    if (pages.length === 0) return null;
    return (
        <div className='pagination-wr' style={{ float: 'right' }}>
            <div class="aa-product-catg-pagination">
                <nav>
                    <ul class="pagination">
                        <li onClick={(e) => {
                            e.stopPropagation();
                            if (page !== 0) onPageClick(page - 1);
                        }}>
                            <a aria-label="Previous">
                                <span aria-hidden="true">&laquo;</span>
                            </a>
                        </li>
                        {pages.map(page => page)}
                        <li onClick={(e) => {
                            if (page + 1 < Math.ceil(total / offset)) onPageClick(page + 1);
                        }}>
                            <a aria-label="Next">
                                <span aria-hidden="true">&raquo;</span>
                            </a>
                        </li>
                    </ul>
                </nav>
            </div >


        </div >
    )
}

export default withRouter(Pagination);