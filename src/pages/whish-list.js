import React, { Fragment, useEffect } from 'react';
import { Link } from 'react-router-dom';
import AppLoader from '../components/shared/app-loader';
import { checkLoading, resolveByType, getImageUrl } from '../utils/util';
import { LIKED_PRODUCTS } from '../utils/constants';
import { toast } from 'react-toastify';

const WhishList = (props) => {
    const { likedProducts, userLikedProductsAction, addToCartAction, removeLikeAction } = props;
    useEffect(() => {
        userLikedProductsAction();
    }, [userLikedProductsAction]);
    const serverCallback = ({ type, message }) => {
        resolveByType({
            type,
            success: () => toast.success('Added to cart'),
            failure: () => toast.error(message)
        })
    }
    const likeCallback = ({ type, message }) => {
        resolveByType({
            type,
            failure: () => toast.error(message)
        })
    }
    return (
        <Fragment>
            <section id="aa-catg-head-banner">
                <img src="img/fashion/fashion-header-bg-8.jpg" alt="fashion img" />
                <div className="aa-catg-head-banner-area">
                    <div className="container">
                        <div className="aa-catg-head-banner-content">
                            <h2>Wishlist Page</h2>
                            <ol className="breadcrumb">
                                <li><Link href="index.html">Home</Link></li>
                                <li className="active">Wishlist</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </section>
            <section id="cart-view">
                <div className="container">
                    <div className="row">
                        <div className="col-md-12">
                            <AppLoader loading={checkLoading(likedProducts)} parent failType='coudnt fetch now'>
                                {likedProducts && likedProducts.length > 0 ?
                                    <div className="cart-view-area">
                                        <div className="cart-view-table aa-wishlist-table">
                                            <form action="">
                                                <div className="table-responsive">
                                                    <table className="table">
                                                        <thead>
                                                            <tr>
                                                                <th></th>
                                                                <th></th>
                                                                <th>Product</th>
                                                                <th>Price</th>
                                                                <th> Status</th>
                                                                <th></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            {likedProducts.map(product => {
                                                                const { title, discount, price, _id, images, inCart } = product.data;
                                                                return (
                                                                    <tr>
                                                                        <td><Link onClick={() => removeLikeAction({ productId: _id, type: LIKED_PRODUCTS }, likeCallback)} className="remove"><fa className="fa fa-close"></fa></Link></td>
                                                                        <td><Link><img src={getImageUrl(images[0])} alt="img" /></Link></td>
                                                                        <td><Link className="aa-cart-title">{title}</Link></td>
                                                                        <td>{price - discount}</td>
                                                                        <td>Available</td>
                                                                        <td><Link onClick={(e) => {
                                                                            e.stopPropagation();
                                                                            if (!inCart) {
                                                                                addToCartAction({ productId: _id, type: LIKED_PRODUCTS }, serverCallback);
                                                                            }
                                                                        }} className="aa-add-to-cart-btn">{inCart ? 'In cart' : 'Add To Cart'}</Link></td>
                                                                    </tr>
                                                                )
                                                            })}
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </form>
                                        </div>
                                    </div> : <div className='text-center'>You dont have any products added to WhishList</div>}
                            </AppLoader>
                        </div>
                    </div>
                </div>
            </section>
        </Fragment>
    )
}

export default WhishList
