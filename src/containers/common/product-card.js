import { connect } from 'react-redux';
import { addToCartAction, removeFromCartAction, likeProductAction, removeLikeAction } from '../../actions/user.actions';
import ProductCard from '../../components/shared/product-card';
import { setProductLens } from '../../redux/common.reducer';
import { featureItemAction, unFeatureItemAction } from '../../actions/admin.actions';

const mapStateToProps = (state) => ({
    loggedIn: state.session.loggedIn,
    admin: state.session.profile && state.session.profile.role === 'admin'
});

const mapDispatchToProps = (dispatch) => ({
    addToCartAction: (productInfo, callback) => dispatch(addToCartAction(productInfo, callback)),
    removeFromCartAction: (productInfo, callback) => dispatch(removeFromCartAction(productInfo, callback)),
    likeProductAction: (productInfo, callback) => dispatch(likeProductAction(productInfo, callback)),
    removeLikeAction: (productInfo, callback) => dispatch(removeLikeAction(productInfo, callback)),
    setProductLens: (productInfo) => dispatch(setProductLens(productInfo)),
    featureItemAction: (productInfo, callback) => dispatch(featureItemAction(productInfo, callback)),
    unFeatureItemAction: (productInfo, callback) => dispatch(unFeatureItemAction(productInfo, callback))
})

export default connect(mapStateToProps, mapDispatchToProps)(ProductCard)