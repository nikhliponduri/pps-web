import { connect } from "react-redux";
import Menu from "../../components/headers/menu";

const mapStateToProps = (state) => ({
    loggedIn: state.session.loggedIn,
    role: state.session.profile && state.session.profile.role
});

export default connect(mapStateToProps)(Menu)