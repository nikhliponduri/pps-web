import { connect } from "react-redux";
import { withRouter } from 'react-router-dom';
import AppHeader from "../../components/shared/app-header";
import { userLogoutAction } from "../../actions/user.actions";

const mapStateToProps = (state) => ({
    loggedIn: state.session.loggedIn,
    role: state.session.profile && state.session.profile.role
});

export default connect(mapStateToProps, { userLogoutAction })(withRouter(AppHeader))