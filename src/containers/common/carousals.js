import { connect } from "react-redux";
import Sliders from "../../components/headers/sliders";

const mapStateToProps = (state) => ({
    carousals: state.common.carousals
});

export default connect(mapStateToProps)(Sliders)