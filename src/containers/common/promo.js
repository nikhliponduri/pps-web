import { connect } from 'react-redux';
import Promo from '../../components/headers/promo';

const mapStateToProps = (state) => ({
    promos: state.common.promos
});

export default connect(mapStateToProps)(Promo)