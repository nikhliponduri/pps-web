import { connect } from 'react-redux';
import { addToCartAction, removeFromCartAction, likeProductAction, removeLikeAction, productDetailAction } from '../../actions/user.actions';
import ProductDetailLoader from '../../pages/product-detail-loader';
import { PRODUCT_DETAIL } from '../../utils/constants';

const mapStateToProps = (state) => ({
    product: state.common.selectedProduct,
    loggedIn: state.session.loggedIn,
    type: PRODUCT_DETAIL
});

const mapDispatchToProps = (dispatch) => ({
    addToCartAction: (productInfo, callback) => dispatch(addToCartAction(productInfo, callback)),
    removeFromCartAction: (productInfo, callback) => dispatch(removeFromCartAction(productInfo, callback)),
    likeProductAction: (productInfo, callback) => dispatch(likeProductAction(productInfo, callback)),
    removeLikeAction: (productInfo, callback) => dispatch(removeLikeAction(productInfo, callback)),
    productDetailAction: (productInfo, callback) => dispatch(productDetailAction(productInfo, callback))
});

export default connect(mapStateToProps, mapDispatchToProps)(ProductDetailLoader);