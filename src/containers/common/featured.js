import { connect } from 'react-redux';
import Featured from '../../components/headers/featured';
import { featuredProductsAction } from '../../actions/user.actions';

const mapStateToProps = (state) => ({
    featuredProducts: state.common.featuredProducts
});

export default connect(mapStateToProps, { featuredProductsAction })(Featured)