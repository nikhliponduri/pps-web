import { connect } from 'react-redux';
import { addToCartAction, removeFromCartAction } from '../../actions/user.actions';
import ProductLens from '../../components/shared/product-lens';

const mapStateToProps = (state) => ({
    product: state.common.productLens,
    loggedIn: state.session.loggedIn,
    role: state.session.profile && state.session.profile.role
});

const mapDispatchToProps = (dispatch) => ({
    addToCartAction: (productInfo, callback) => dispatch(addToCartAction(productInfo, callback)),
    removeFromCartAction: (productInfo, callback) => dispatch(removeFromCartAction(productInfo, callback))
});

export default connect(mapStateToProps, mapDispatchToProps)(ProductLens);