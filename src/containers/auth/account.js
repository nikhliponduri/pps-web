import { connect } from 'react-redux';
import { userRegisterAction, userLoginAction } from '../../actions/user.actions';
import Account from '../../pages/account';

export default connect(null, { userRegisterAction, userLoginAction })(Account);