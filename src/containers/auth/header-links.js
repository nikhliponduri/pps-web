import { connect } from 'react-redux';
import HeaderLinks from '../../components/headers/header-links';
import { userLogoutAction } from '../../actions/user.actions';

const mapStateToProps = (state) => ({
    loggedIn: state.session.loggedIn,
    role: state.session.profile && state.session.profile.role
});

export default connect(mapStateToProps, { userLogoutAction })(HeaderLinks);