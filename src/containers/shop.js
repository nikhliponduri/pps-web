import { connect } from 'react-redux';
import { allProductsAction } from '../actions/user.actions';
import Shop from '../pages/shop';

const mapStateToProps = (state) => ({
    allProducts: state.common.allProducts
});

const mapDispatchToProps = (dispatch) => ({
    allProductsAction: (info, callback) => dispatch(allProductsAction(info, callback))
});

export default connect(mapStateToProps, mapDispatchToProps)(Shop)