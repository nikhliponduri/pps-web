import { connect } from 'react-redux';
import { addToCartAction, removeFromCartAction, removeLikeAction, userLikedProductsAction } from '../actions/user.actions';
import WhishList from '../pages/whish-list';

const mapStateToProps = (state) => ({
    likedProducts: state.user.likedProducts
});

const mapDispatchToProps = (dispatch) => ({
    addToCartAction: (productInfo, callback) => dispatch(addToCartAction(productInfo, callback)),
    removeFromCartAction: (productInfo, callback) => dispatch(removeFromCartAction(productInfo, callback)),
    removeLikeAction: (productInfo, callback) => dispatch(removeLikeAction(productInfo, callback)),
    userLikedProductsAction: (callback) => dispatch(userLikedProductsAction(callback))
});

export default connect(mapStateToProps, mapDispatchToProps)(WhishList);