import { connect } from 'react-redux';
import { allProductsAction } from '../actions/user.actions';
import Search from '../pages/search';
import { ALL_PRODUCTS } from '../utils/constants';

const mapStateToProps = (state) => ({
    allProducts: state.common.allProducts,
    type: ALL_PRODUCTS,
    loggedIn:state.session.loggedIn
});

const mapDispatchToProps = (dispatch) => ({
    allProductsAction: (info, callback) => dispatch(allProductsAction(info, callback))
});

export default connect(mapStateToProps, mapDispatchToProps)(Search);