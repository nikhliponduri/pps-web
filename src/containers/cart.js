import { connect } from 'react-redux';
import { removeFromCartAction } from '../actions/user.actions';
import Cart from '../pages/cart';

const mapStateToProps = (state) => ({
    cart: state.user.cart
});

const mapDispatchToProps = (dispatch) => ({
    removeFromCartAction: (productInfo, callback) => dispatch(removeFromCartAction(productInfo, callback))
});

export default connect(mapStateToProps, mapDispatchToProps)(Cart);