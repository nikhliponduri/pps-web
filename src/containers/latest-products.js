import { connect } from 'react-redux';
import { latestProductsAction } from '../actions/user.actions';
import LatestProducts from '../pages/latest-products';
import { LATEST_PRODUCTS } from '../utils/constants';

const mapStateTorops = state => ({
    latestProducts: state.common.latestProducts,
    type: LATEST_PRODUCTS,
    loggedIn:state.session.loggedIn
});

const mapDispatchTopProps = (dispatch) => ({
    latestProductsAction: (callback) => dispatch(latestProductsAction(callback))
});

export default connect(mapStateTorops, mapDispatchTopProps)(LatestProducts);