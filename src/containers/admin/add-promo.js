import { connect } from 'react-redux';
import { addPromoAction } from '../../actions/admin.actions';
import AddPromo from '../../pages/admin/add-promo';

export default connect(null, { addPromoAction })(AddPromo)
