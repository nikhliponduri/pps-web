import { connect } from 'react-redux';
import AddProduct from '../../pages/admin/add-product';
import { addProductAction } from '../../actions/admin.actions';

export default connect(null, {addProductAction })(AddProduct)