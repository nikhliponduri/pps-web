import { connect } from 'react-redux';
import { removeFromCartAction } from '../actions/user.actions';
import HeaderCart from '../components/headers/header-cart';

const mapStateToProps = (state) => ({
    cart: state.user.cart,
    loggedIn: state.session.loggedIn
});

const mapDispatchToProps = (dispatch) => ({
    removeFromCartAction: (productInfo, callback) => dispatch(removeFromCartAction(productInfo, callback))
});

export default connect(mapStateToProps, mapDispatchToProps)(HeaderCart);