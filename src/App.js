import React from 'react';
import { toast, ToastContainer } from 'react-toastify';

import 'react-toastify/dist/ReactToastify.css';
import AppHeader from './components/headers/app-header';
import Menu from './containers/common/menu';
import DefaultHTMLS from './pages/defaultHTMLS';
import AppRouter from './containers/common/app-router';

function App(props) {
  toast.configure();
  const { loading } = props;
  if (loading) {
    return (
      <div id="wpf-loader-two">
        <div class="wpf-loader-two-inner">
          <span>Loading</span>
        </div>
      </div>
    )
  }
  return (
    <div>
      <ToastContainer />
      <DefaultHTMLS />
      <AppHeader />
      <Menu />
      <AppRouter />
    </div>
  );
}

export default App;
