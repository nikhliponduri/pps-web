import React from 'react'
import Sliders from './headers/sliders';
import Featured from '../containers/common/featured';
import LatestProducts from '../containers/latest-products';
import Promo from '../containers/common/promo';
import AllProducts from '../containers/all-products';

const Home = () => {
    return (
        <div>
            <Sliders />
            <Featured />
            <Promo />
            <LatestProducts />
            <AllProducts />
        </div>
    )
}

export default Home
