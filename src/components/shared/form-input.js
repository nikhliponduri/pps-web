import React, { Fragment } from 'react'

const FormInput = ({ type, className, style = {}, onChange, error, placeholder, value = '', name, id }) => {
    return (
        <Fragment>
            <input id={id} name={name} className={className} type={type} style={style} onChange={onChange} placeholder={placeholder} value={value} />
            {error && <span style={{ color: 'red' }}>{error}</span>}
        </Fragment>
    )
}

export default FormInput
