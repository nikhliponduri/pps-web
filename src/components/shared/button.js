import React from 'react'

const Button = ({ className, loading, disabled, title, style = {}, id, onClick }) => {
    return (
        <button onClick={onClick} id={id} className={className} disabled={disabled} style={style} loading={loading}>{title}</button>
    )
}

export default Button
