import React, { Fragment, useState } from 'react';
import { Link } from 'react-router-dom';
import { S3_URL } from '../../utils/constants';
import { resolveByType, clickLogin } from '../../utils/util';
import { toast } from 'react-toastify';

const ProductCard = (props) => {
    const { product: { _id, images, price, discount, liked, inCart, title, featured }, product, addToCartAction, removeFromCartAction, likeProductAction, removeLikeAction, loggedIn, type, setProductLens, featureItemAction, unFeatureItemAction, admin } = props;
    const [loading, setLoading] = useState(false);
    const serverCallback = ({ type, message }) => {
        setLoading(false);
        resolveByType({
            type,
            failure: () => {
                toast.error(message)
            }
        });
    }
    const handleView = (e) => {
        e.preventDefault();
        setProductLens(product);
    }
    const handleLike = (e) => {
        e.preventDefault();
        if (!liked) return likeProductAction({
            productId: _id,
            type
        }, serverCallback);
        else removeLikeAction({
            productId: _id,
            type
        }, serverCallback);
    }
    const cartCallback = ({ type, message }) => {
        resolveByType({
            type,
            failure: () => toast.error(message)
        })
    }
    const handleCart = (e) => {
        e.preventDefault();
        if (!loggedIn) return clickLogin();
        e.stopPropagation();
        setLoading(true);
        if (admin) {
            if (!featured) return featureItemAction({ productId: _id, type }, serverCallback);
            return unFeatureItemAction({ productId: _id, type }, serverCallback)
        }
        if (inCart) return removeFromCartAction({ productId: _id, type }, cartCallback);
        addToCartAction({ productId: _id, type }, cartCallback);
    }
    return (
        <li className='image-list'>
            <figure>
                <Link onClick={(e) => e.preventDefault()} className="aa-product-img"><img src={`${S3_URL}/${images[0]}`} alt="img" /></Link>
                <Link className={`aa-add-card-btn ${inCart && 'bg-danger'}`} onClick={handleCart}>
                    <span className="fa fa-shopping-cart"></span>{admin ? (featured ? 'UnFeature' : 'Feature') : (inCart ? 'Remove From Cart' : 'Add To Cart')}
                </Link>
                <figcaption>
                    <h4 className="aa-product-title"><Link to={`/product-detail/${_id}`}>{title}</Link></h4>
                    <span className="aa-product-price">{price}</span>{discount && <span className="aa-product-price"><del>{discount}</del></span>}
                </figcaption>
            </figure>
            <div className="aa-product-hvr-content">

                <Fragment>
                    <Link onClick={handleLike} data-toggle="tooltip" data-placement="top" title="Add to Wishlist"><span className={`fas fa-heart ${liked && 'red'}`}></span></Link>
                    {/* <Link data-toggle="tooltip" data-placement="top" title="Compare"><span className="fa fa-exchange"></span></Link> */}
                    <Link onClick={handleView} data-toggle2="tooltip" data-placement="top" title="Quick View" data-toggle="modal" data-target="#quick-view-modal"><span className="fa fa-search"></span></Link>
                </Fragment>

            </div>
            {/* <!-- product badge --> */}
            <span className="aa-badge aa-sale" href="#">SALE!</span>
        </li>
    )
}

export default ProductCard
