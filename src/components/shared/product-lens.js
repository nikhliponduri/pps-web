import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { getImageUrl } from '../../utils/util';

class ProductLens extends Component {
    componentDidMount() {
        window.lensFunction && window.lensFunction();
        window.changeLensImage && window.changeLensImage();
    }
    componentDidUpdate() {
        const { product = {} } = this.props;
        const { images = [] } = product;
        window.lensFunction && window.lensFunction();
        document.getElementsByClassName('simpleLens-big-image')[0].src = getImageUrl(images[0]);
        document.getElementsByClassName('simpleLens-lens-image')[0]['data-lens-image'] = getImageUrl(images[0]);
    }
    setImage(e, image) {
        e.preventDefault();
        document.getElementById('data-change-image').src = getImageUrl(image);
    }
    closeModal() {
        document.getElementById('modal-close').click();
    }
    render() {
        const { product = {} } = this.props;
        const { images = [], title = '', price, discount, description = '', artist = '', _id } = product;
        return (
            <div className="modal fade" id="quick-view-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div className="modal-dialog">
                    <div className="modal-content">
                        <div className="modal-body">
                            <button type="button" id='modal-close' className="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <div className="row">
                                {/* <!-- Modal view slider --> */}
                                <div className="col-md-6 col-sm-6 col-xs-12">
                                    <div className="aa-product-view-slider">
                                        <div className="simpleLens-gallery-container" id="demo-1">
                                            <div className="simpleLens-container">
                                                <div className="simpleLens-big-image-container">
                                                    <Link className="simpleLens-lens-image">
                                                        <img id='data-change-image' src={getImageUrl(images[0])} className="simpleLens-big-image" alt={title} />
                                                    </Link>
                                                </div>
                                            </div>
                                            <div className="simpleLens-thumbnails-container">
                                                {
                                                    images.map(image => <Link onClick={(e) => this.setImage(e, image)} className="simpleLens-thumbnail-wrapper"
                                                        data-lens-image={getImageUrl(image)}
                                                        data-big-image={getImageUrl(image)}>
                                                        <img src={getImageUrl(image)} alt={title} />
                                                    </Link>)
                                                }
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {/* <!-- Modal view content --> */}
                                <div className="col-md-6 col-sm-6 col-xs-12">
                                    <div className="aa-product-view-content">
                                        <h3>{title}</h3>
                                        <div className="aa-price-block">
                                            <span className="aa-product-view-price">{price - discount}</span>
                                            {/* <p className="aa-product-avilability">Avilability: <span>In stock</span></p> */}
                                        </div>
                                        <p>{description}</p>
                                        {/* <h4>Size</h4>
                                        <div className="aa-prod-view-size">
                                            <Link href="#">S</Link>
                                            <Link href="#">M</Link>
                                            <Link href="#">L</Link>
                                            <Link href="#">XL</Link>
                                        </div> */}
                                        <div className="aa-prod-quantity">
                                            {/* <form action="">
                                                <select name="" id="">
                                                    <option value="0" selected="1">1</option>
                                                    <option value="1">2</option>
                                                    <option value="2">3</option>
                                                    <option value="3">4</option>
                                                    <option value="4">5</option>
                                                    <option value="5">6</option>
                                                </select>
                                            </form> */}
                                            <p className="aa-prod-category">
                                                Art By <Link href="#">{artist}</Link>
                                            </p>
                                        </div>
                                        <div className="aa-prod-view-bottom">
                                            <Link onClick={this.closeModal} to={`/product-detail/${_id}`} className="aa-add-to-cart-btn">View Details</Link>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {/* <!-- /.modal-content --> */}
                </div>
                {/* <!-- /.modal-dialog --> */}
            </div>

        )
    }
}
export default ProductLens
