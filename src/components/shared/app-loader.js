import React, { Fragment } from 'react';
import Loader from 'react-loader-spinner';

const AppLoader = ({ height, width, type = "ThreeDots", color, children, loading, failType = '', parent = false }) => {
    if (parent) {
        if (loading === null) {
            return typeof (failType) === 'function' ? failType() : failType;
        }
        if (loading) return (
            <div className='app-loader'>
                <Loader
                    type="Bars"
                    color="#002C4D"
                    height="50"
                    width="80" />
            </div>
        )
        return <Fragment>{children}</Fragment>
    }
    return (
        <Loader height={height} width={width} color={color} type={type} />
    )
}

export default AppLoader
