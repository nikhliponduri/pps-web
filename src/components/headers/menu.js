import React,{Fragment} from 'react';
import {Link} from 'react-router-dom';
import { clickLogin } from '../../utils/util';

const Menu = ({loggedIn,role}) => {
    return (
        <section id="menu">
            <div className="container">
                <div className="menu-area">
                    {/* <!-- Navbar --> */}
                    <div className="navbar navbar-default" role="navigation">
                        <div className="navbar-header">
                            <button type="button" className="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                <span className="sr-only">Toggle navigation</span>
                                <span className="icon-bar"></span>
                                <span className="icon-bar"></span>
                                <span className="icon-bar"></span>
                            </button>
                        </div>
                        <div className="navbar-collapse collapse">
                            {/* <!-- Left nav --> */}
                            <ul className="nav navbar-nav">
                                <li><Link to='/' >Home</Link></li>
                                {
                                    !loggedIn ? 
                                    <Fragment>
                                        <li><Link onClick={(e)=>{
                                            e.stopPropagation();
                                            clickLogin();
                                        }} >Login</Link></li>
                                    </Fragment>
                                    : (role==='admin'?
                                    <Fragment>
                                        <li><Link to='/add-product-single'>add Product</Link> </li>
                                        <li><Link to='/add-promo'>add Promo</Link> </li>
                                        <li><Link to='/add-banner'>add Banner</Link> </li>
                                    </Fragment>:
                                    <Fragment>
                                        <li><Link to='/whish-list'>Wishlist</Link> </li>
                                    </Fragment>
                                    )
                                }
                            </ul>
                        </div>
                        {/* <!--/.nav-collapse --> */}
                    </div>
                </div>
            </div>
        </section>
    )
}

export default Menu
