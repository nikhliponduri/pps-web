import React from 'react';
import { Link } from 'react-router-dom';
import AppLoader from '../shared/app-loader';
import { checkLoading, getDiscountPercentage, getImageUrl } from '../../utils/util';

const Promo1 = (props) => {
    const { promo } = props;
    if (!promo) return null;
    const { data: { title, price, discount, images, _id } } = promo;
    return <div className="col-md-5 no-padding">
        <div className="aa-promo-left">
            <div className="aa-promo-banner">
                <img src={getImageUrl(images[0])} alt={title} />
                <div className="aa-prom-content">
                    {discount && <span>{getDiscountPercentage(price, discount)}% Off</span>}
                    <h4><Link to={`/product-detail/${_id}`}>{title}</Link></h4>
                </div>
            </div>
        </div>
    </div>
}

const Promo2 = (props) => {
    const { promo } = props;
    if (!promo) return null;
    const { data: { title, price, discount, images, _id } } = promo;
    return <div className="aa-single-promo-right">
        <div className="aa-promo-banner">
            <img src={getImageUrl(images[0])} alt="img" />
            <div className="aa-prom-content">
                <span>{getDiscountPercentage(price, discount)}% Off</span>
                <h4><Link to={`/product-detail/${_id}`}>{title}</Link></h4>
            </div>
        </div>
    </div>
}


const Promo = ({ promos }) => {
    return (
        <section id="aa-promo">
            <div className="container">
                <AppLoader loading={checkLoading(promos)} parent>
                    <div className="row">
                        <div className="col-md-12">
                            {promos && <div className="aa-promo-area">
                                <div className="row">
                                    {/* <!-- promo left --> */}
                                    <Promo1 promo={promos.find(promo => promo.promo === 1)} />
                                    {/* <!-- promo right --> */}
                                    <div className="col-md-7 no-padding">
                                        <div className="aa-promo-right">
                                            <Promo2 promo={promos.find(promo => promo.promo === 2)} />
                                            <Promo2 promo={promos.find(promo => promo.promo === 3)} />
                                            <Promo2 promo={promos.find(promo => promo.promo === 4)} />
                                            <Promo2 promo={promos.find(promo => promo.promo === 5)} />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            }
                        </div>
                    </div>
                </AppLoader>
            </div>
        </section>
    )
}

export default Promo
