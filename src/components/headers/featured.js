import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { getImageUrl } from '../../utils/util';

class Featured extends Component {
    componentWillMount() {
        const { featuredProductsAction } = this.props;
        featuredProductsAction();
    }
    componentDidMount() {
        const { featuredProducts } = this.props;
        if (Array.isArray(featuredProducts) && featuredProducts.length) {
            window.clientBrandSlider && window.clientBrandSlider();
        }
    }
    componentDidUpdate() {
        const { featuredProducts } = this.props;
        if (Array.isArray(featuredProducts) && featuredProducts.length) {
            window.clientBrandSlider && window.clientBrandSlider()
        }
    }
    render() {
        const { featuredProducts } = this.props;
        if (!Array.isArray(featuredProducts)) return null;
        return (
            <section id="aa-client-brand" style={{ marginTop: '40px' }} >
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="aa-client-brand-area">
                                <ul class="aa-client-brand-slider">
                                    {featuredProducts && featuredProducts.map(product => {
                                        const { _id, images } = product.data;
                                        return <li>
                                            <Link to={`/product-detail/${_id}`}>
                                                <img src={getImageUrl(images[0])} alt="img" />
                                            </Link>
                                        </li>
                                    })}
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </section >
        )
    }
}

export default Featured
