import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import AppLoader from '../shared/app-loader';
import { checkLoading, getImageUrl } from '../../utils/util';

class HeaderCart extends Component {
    componentDidMount() {
        window.cartHover && window.cartHover();
    }
    singleItem = (cart) => {
        const { _id, title, price, discount, images } = cart.data;
        const { removeFromCartAction } = this.props
        return <li>
            <Link className="aa-cartbox-img" to={`/product-detail/${_id}`}><img src={getImageUrl(images[0])} alt="img" /></Link>
            <div className="aa-cartbox-info">
                <h4><Link href="#">{title}</Link></h4>
                <p>1 x {price - discount}</p>
            </div>
            <Link onClick={() => removeFromCartAction({ productId: _id, type: 'ALL' })} className="aa-remove-product"><span className="fa fa-times"></span></Link>
        </li>
    }
    render() {
        const { loggedIn, cart } = this.props
        if (!loggedIn) return '';
        return (
            <div className="aa-cartbox">
                <Link className="aa-cart-link" to='/cart'>
                    <span className="fa fa-shopping-basket"></span>
                    <span className="aa-cart-title">SHOPPING CART</span>
                    <span className="aa-cart-notify">{cart && cart.length}</span>
                </Link>
                <div className="aa-cartbox-summary">
                    <AppLoader loading={checkLoading(cart)} failType='Couldnt fetch now' parent>
                        <ul>
                            {cart && cart.length > 0 ? cart.map(product => this.singleItem(product)) : 'Your shopping cart is empty'}
                        </ul>
                    </AppLoader>
                    {/* {cart && cart.length > 0 && <Link className="aa-cartbox-checkout aa-primary-btn" href="checkout.html">Checkout</Link>} */}
                </div>
            </div >

        )
    }
}

export default HeaderCart;
