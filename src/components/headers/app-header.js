import React, { useState } from 'react';
import { withRouter } from 'react-router-dom';
import { Link } from 'react-router-dom';
import HeaderLinks from '../../containers/auth/header-links';
import HeaderCart from '../../containers/header-cart';

const AppHeader = ({ history: { push } }) => {
    const [search, setSearch] = useState('');
    return (
        <header id="aa-header">
            {/* <!-- start header top  --> */}
            <div className="aa-header-top">
                <div className="container">
                    <div className="row">
                        <div className="col-md-12">
                            <div className="aa-header-top-area">
                                {/* <!-- start header top left --> */}
                                <div className="aa-header-top-left">
                                    {/* <!-- start language --> */}
                                    <div className="aa-language">
                                        <div className="dropdown">
                                            <span className="btn dropdown-toggle" href="#" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                                <img src="img/flag/english.jpg" alt="english flag" />ENGLISH
                                                    <span className="caret"></span>
                                            </span>
                                            {/* <ul className="dropdown-menu" aria-labelledby="dropdownMenu1">
                                                <li><Link href="#"><img src="img/flag/french.jpg" alt="" />FRENCH</Link></li>
                                                <li><Link href="#"><img src="img/flag/english.jpg" alt="" />ENGLISH</Link></li>
                                            </ul> */}
                                        </div>
                                    </div>
                                    {/* <!-- / language -->

                <!-- start currency --> */}
                                    <div className="aa-currency">
                                        <div className="dropdown">
                                            <span className="btn dropdown-toggle" href="#" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                            <i class="fas fa-rupee-sign"></i>INR
                                             <span className="caret"></span>
                                            </span>
                                            {/* <ul className="dropdown-menu" aria-labelledby="dropdownMenu1">
                                                <li><Link href="#"><i className="fa fa-euro"></i>EURO</Link></li>
                                                <li><Link href="#"><i className="fa fa-jpy"></i>YEN</Link></li>
                                            </ul> */}
                                        </div>
                                    </div>
                                    {/* <!-- / currency -->
                <!-- start cellphone --> */}
                                    <div className="cellphone hidden-xs">
                                        <p><span className="fa fa-phone"></span>91 7674088054</p>
                                    </div>
                                    {/* <!-- / cellphone --> */}
                                </div>
                                {/* <!-- / header top left --> */}
                                <HeaderLinks />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {/* <!-- / header top  --> */}

            {/* <!-- start header bottom  --> */}
            <div className="aa-header-bottom">
                <div className="container">
                    <div className="row">
                        <div className="col-md-12">
                            <div className="aa-header-bottom-area">
                                {/* <!-- logo  --> */}
                                <div className="aa-logo">
                                    {/* <!-- Text based logo --> */}
                                    <Link to='/'>
                                        <span className="fa fa-paint-brush"></span>
                                        <p>PAINT <strong> .</strong> <span>Your Painting Partner</span></p>
                                    </Link>
                                    {/* <!-- img based logo --> */}
                                    {/*  <Link href="index.html"><img src="img/logo.jpg" alt="logo img"/></Link>  */}
                                </div>
                                {/* <!-- / logo  -->
               <!-- cart box --> */}
                                <HeaderCart />
                                {/* <!-- / cart box -->
              <!-- search box --> */}
                                <div className="aa-search-box">
                                    <form action="">
                                        <input onChange={(e) => setSearch(e.target.value)} type="text" name="" id="" placeholder="Search here  " />
                                        <button type="submit" onClick={(e) => {
                                            e.preventDefault();
                                            push(`/search?name=${search}`)
                                        }}><span className="fa fa-search"></span></button>
                                    </form>
                                </div>
                                {/* <!-- / search box -->              */}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {/* <!-- / header bottom  --> */}
        </header>
    )
}

export default withRouter(AppHeader);
