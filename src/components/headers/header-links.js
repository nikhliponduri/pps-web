import React, { Fragment } from 'react';
import { Link } from 'react-router-dom';

const HeaderLinks = ({ loggedIn, role, userLogoutAction }) => {
    return (
        <div className="aa-header-top-right">
            <ul className="aa-head-top-nav-right">
                {!loggedIn && <li><Link to='/account'>My Account</Link></li>}
                {role === 'user' &&
                    <Fragment>
                        <li className="hidden-xs"><Link to='/whish-list'>Wishlist</Link></li>
                        <li className="hidden-xs"><Link to='/cart'>My Cart</Link></li>
                        <li className="hidden-xs"><Link to='/checkout'>Checkout</Link></li>
                    </Fragment>
                }
                {!loggedIn && <li><span style={{
                    display: 'inline-block',
                    color: '#333333',
                    borderRight: '1px solid #ddd',
                    fontSize: '14px',
                    padding: '5px 8px'
                }} className='pointer text-center' id='login-box' data-toggle="modal" data-target="#login-modal">Login</span></li>}
                {loggedIn && <li><span onClick={(e) => userLogoutAction()} style={{
                    display: 'inline-block',
                    color: '#333333',
                    borderRight: '1px solid #ddd',
                    fontSize: '14px',
                    padding: '5px 8px'
                }} className='pointer'>Logout</span></li>}
            </ul>
        </div>
    )
}

export default HeaderLinks
