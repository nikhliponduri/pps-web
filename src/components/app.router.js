import React from 'react';
import { Route, Switch } from 'react-router-dom';
import Home from './home';
import NotFound from '../pages/not-found';
import account from '../containers/auth/account';
import AuthRoute from './auth-route';
import addPromo from '../containers/admin/add-promo';
import addProduct from '../containers/admin/add-product';
import productDetail from '../containers/common/product-detail';
import cart from '../containers/cart';
import whishList from '../containers/whish-list';
import shop from '../containers/shop';
import search from '../containers/search';

const AppRouter = ({ role, loggedIn }) => {
    return (
        <Switch>
            <Route exact path='/' component={Home} />
            <Route exact path='/product-detail/:productId' component={productDetail} />
            {/* <Route exact path='/shop' component={shop} /> */}
            <Route exact path='/search' component={search} />
            <AuthRoute path='/account' exact component={account} loggedIn={!loggedIn} isValidRole={!loggedIn} />
            <AuthRoute path='/add-promo' isValidRole={role === 'admin'} loggedIn={loggedIn} component={addPromo} />
            <AuthRoute path='/add-product-single' loggedIn={loggedIn} isValidRole={role === 'admin'} component={addProduct} />
            <AuthRoute path='/cart' exact component={cart} loggedIn={loggedIn} isValidRole={loggedIn} />
            <AuthRoute path='/whish-list' exact component={whishList} loggedIn={loggedIn} isValidRole={loggedIn} />
            <Route path='/*' component={NotFound} />
        </Switch>
    )
}

export default AppRouter
